#!/bin/env python
import sys
import pwn
from itertools import islice
from collections import defaultdict
from functools import partial
import json


def choose_type(io, type_):
    io.sendlineafter(b"2. int\n", str(type_).encode())


def generate_hundred(io, size):
    io.sendlineafter("9. Wyjdź\n".encode(), b"4")
    io.sendlineafter(
        "Podaj liczbę liczb do wygnerowania: ".encode(), str(size).encode()
    )


def generate_30_60(io, size):
    io.sendlineafter("9. Wyjdź\n".encode(), b"2")
    io.sendlineafter(
        "Podaj liczbę liczb do wygnerowania: ".encode(), str(size).encode()
    )


def generate_60_30(io, size):
    io.sendlineafter("9. Wyjdź\n".encode(), b"3")
    io.sendlineafter(
        "Podaj liczbę liczb do wygnerowania: ".encode(), str(size).encode()
    )


def sort(io, type_idx):
    io.sendlineafter("9. Wyjdź\n".encode(), str(8).encode())
    io.sendlineafter(
        "8. Quick sort (środkowy element pivotem)\n".encode(),
        str(type_idx).encode(),
    )
    time_ns = int(
        io.recvuntil(b"1. Wczytaj plik").split(b"\n")[0].split(b" ")[-1]
    )
    return time_ns


def check_sort(io):
    io.sendlineafter("9. Wyjdź\n".encode(), b"7")
    data = io.recvuntil("Wybierz opcję:\n".encode())
    if b"Poprawny wynik" not in data:
        raise ValueError("Spierdoliło się na amen")

def exit(io):
    io.sendlineafter("9. Wyjdź\n".encode(), b"9")

def size_iter(init):
    past = init
    while True:
        past += init
        yield past


if __name__ == "__main__":
    if not len(sys.argv) > 1:
        print("Usage: ./test.py [binary_path]")
        sys.exit(0)

    sort_choices = [
        "insertion",
        "heap",
        "shell_shell",
        "shell_knuth",
        "quick_first",
        "quick_last",
        "quick_random",
        "quick_middle",
    ]

    binary = sys.argv[1]
    # pwn.context.log_level = "debug"
    pwn.context.timeout = pwn.Timeout.forever
    # io = pwn.gdb.debug(binary)
    io = pwn.process(binary)
    for type in range(2, 3):
        choose_type(io, type)
        res_dict = {}

        init_size = 10000
        for size in islice(size_iter(init_size), 0, 7):
            res_dict[size] = {}
            res_dict[size]["100"] = defaultdict(list)
            res_dict[size]["30_60"] = defaultdict(list)
            res_dict[size]["60_30"] = defaultdict(list)
            print(type, "testing size: ", size)
            for i in range(100):
                print("iteration", i)
                generate_hundred(io, size)
                for i, method in enumerate(sort_choices, 1):
                    if i!= 5:
                        continue
                    time_ns = sort(io, i)
                    check_sort(io)
                    res_dict[size]["100"][method].append(time_ns)
                generate_30_60(io, size)
                for i, method in enumerate(sort_choices, 1):
                    if i!= 5:
                        continue
                    time_ns = sort(io, i)
                    check_sort(io)
                    res_dict[size]["30_60"][method].append(time_ns)
                generate_60_30(io, size)
                for i, method in enumerate(sort_choices, 1):
                    if i!= 5:
                        continue
                    time_ns = sort(io, i)
                    check_sort(io)
                    res_dict[size]["60_30"][method].append(time_ns)
            with open(f"output/{type}result.json", "w") as f:
                f.write(json.dumps(res_dict))
        exit(io)


                    


    io.interactive()
