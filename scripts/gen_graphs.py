#!/bin/env python
import json
import sys
from statistics import mean
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np

with open("output/1result.json", "r") as f:
    data = json.loads(f.read())

type_ = sys.argv[1]
methods = sys.argv[2]

data_res = defaultdict(list)
sizes = []
for size, values in data.items():
    for method in methods.split(","):
        average = mean(values[type_][method]) * 10**(-6)
        data_res[method].append(average)
    sizes.append(size)

fig, ax = plt.subplots()
for data in data_res.values():
    ax.plot(sizes, data, color=np.random.rand(3))
ax.set_ylabel("Czas wykonywania (ms)")
ax.set_xlabel("Rozmiar tablicy")
plt.show()
