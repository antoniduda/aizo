import json
import sys
from statistics import mean, stdev
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np

with open("output/1result.json", "r") as f:
    data = json.loads(f.read())

type_ = sys.argv[1]
methods = sys.argv[2]

data_res = defaultdict(list)
sizes = []
for size, values in data.items():
    for method in methods.split(","):
        average = round(mean(values[type_][method]) * 10**(-6), 2)
        std = round(stdev(values[type_][method]) * 10**(-6), 2)
        data_res[method].append((average, std))
    sizes.append(size)

with open(f"output/table{type_}.csv", "w") as f:
    columns = []
    for column in data_res.keys():
        columns.append(f"T{column} (ms)")
        columns.append(f"S{column} (ms)")
    columns.append("Rozmiar")
    f.write(",".join(columns) + "\n")
    for i, size in enumerate(sizes):
        row = []
        for value in data_res.values():
            row.append(value[i][0])
            row.append(value[i][1])
        row.append(size)
        f.write(",".join(map(str, row)) + "\n")

