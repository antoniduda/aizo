#include "type_menu.h"
#include <fstream>
#include <ios>
#include <iostream>
#include <limits>

class TypeMenuFloat : public TypeMenu<float> {
public:
  TypeMenuFloat() {
    this->max = 100000000.f;
    this->min = -100000000.f;
  }
  void readFile() override {
    std::string filename;
    std::cout << "Podaj nazwę pliku: ";
    std::cin >> filename;
    std::fstream file(filename, std::ios_base::in);
    size_t elem_num;
    file >> elem_num;
    this->data.clear();
    for (int i = 0; i < elem_num; i++) {
      float tmp;
      file >> tmp;
      this->data.push_back(tmp);
    }
  };

  float generate_random(float low, float high) override {
    return low + static_cast<float>(rand()) /
                     (static_cast<float>(RAND_MAX / (high - low)));
  }
};
