#include "type_menu.h"
#include <fstream>
#include <ios>
#include <iostream>
#include <limits>

class TypeMenuInt : public TypeMenu<int> {
public:
  TypeMenuInt() {
    this->max = 100000000;
    this->min = -100000000;
  }
  void readFile() override {
    std::string filename;
    std::cout << "Podaj nazwę pliku: ";
    std::cin >> filename;
    std::fstream file(filename, std::ios_base::in);
    size_t elem_num;
    file >> elem_num;
    this->data.clear();
    for (int i = 0; i < elem_num; i++) {
      int tmp;
      file >> tmp;
      this->data.push_back(tmp);
    }
  };

  int generate_random(int low, int high) override {
    return low + (rand() % (high - low));
  }
};
