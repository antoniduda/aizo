#include "type_menu_float.h"
#include "type_menu_int.h"
#include <time.h>

using namespace std;

bool cmp(int child, int parent) { return parent < child; }

int main() {
  srand(time(NULL));
  while (true) {
    std::cout << "Wybierz typ danych\n";
    std::cout << "1. float\n";
    std::cout << "2. int\n";
    int choice;
    std::cin >> choice;
    TypeMenuFloat t_float;
    TypeMenuInt t_int;
    switch (choice) {
    case 1:
      t_float.menu();
      break;
    case 2:
      t_int.menu();
      break;
    default:
      return 0;
    }
  }
}
