#include <iostream>

#pragma once

template <typename T> struct RefCounter {
  size_t count;
  bool is_array;
};

template <typename T> class RefPtr {
public:
  RefPtr(T *ptr = nullptr, size_t size = 0);
  RefPtr(RefPtr<T> &other);
  RefPtr(RefPtr<T> &&other);
  ~RefPtr();
  RefPtr<T> at(size_t s);
  void operator=(RefPtr<T> other);
  T operator[](size_t i) const;
  T &operator[](size_t i);
  template <class... Args> static RefPtr<T> make(size_t size, Args &&...args);

private:
  RefCounter<T> *ref;
  size_t start = 0;
  T *p_data;
};

template <typename T> RefPtr<T> RefPtr<T>::at(size_t s) {
  RefPtr<T> ret((*this));
  ret.start += s;
  return ret;
}

template <typename T> RefPtr<T>::RefPtr(T *ptr, size_t size) {
  ref = new RefCounter<T>;
  ref->count = 1;
  ref->is_array = (bool)size;
  p_data = ptr;
}

template <typename T> RefPtr<T>::RefPtr(RefPtr<T> &other) {
  ref = other.ref;
  ref->count++;
  p_data = other.p_data;
  start = other.start;
}

template <typename T> RefPtr<T>::RefPtr(RefPtr<T> &&other) {
  ref = other.ref;
  p_data = other.p_data;
  start = other.start;
  delete &other;
}

template <typename T> RefPtr<T>::~RefPtr() {
  ref->count--;
  if (ref->count != 0)
    return;

  if (ref->is_array)
    delete[] p_data;
  else
    delete p_data;

  delete ref;
  return;
}

template <typename T> T RefPtr<T>::operator[](size_t i) const {
  return p_data[start + i];
}
template <typename T> T &RefPtr<T>::operator[](size_t i) {
  return p_data[start + i];
}

template <typename T> void RefPtr<T>::operator=(RefPtr<T> other) {
  this->~RefPtr();
  ref = other.ref;
  ref->count++;
  p_data = other.p_data;
  start = other.start;
}

template <typename T>
template <class... Args>
RefPtr<T> RefPtr<T>::make(size_t size, Args &&...args) {
  if (size) {
    return RefPtr<T>(new T[size], size);
  }
  return RefPtr<T>(new T(&args...));
}
