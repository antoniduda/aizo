
template <typename T>
static void interval_insertion(DArray<T> to_sort, size_t interval) {
  // this is really similar to normal insertion except it
  // works in intervals
  for (size_t i = interval; i < to_sort.get_size(); i += interval) {
    for (size_t j = i; j - interval + 1 > 0 && to_sort[j] < to_sort[j - interval]; j--) {
      T tmp = to_sort[j];
      to_sort[j] = to_sort[j - interval];
      to_sort[j - interval] = tmp;
    }
  }
}

template <typename T> void shell_shell(DArray<T> to_sort) {
  // standard shell intervals reducing the size by 2 each time
  size_t interval = to_sort.get_size() / 2;
  for (; interval > 0; interval /= 2) {
      interval_insertion(to_sort, interval);
  }
}

template <typename T> void shell_knuth(DArray<T> to_sort) {
  // Knuth's shell intervals
  size_t interval = 1;
  // calculate maximum interval lower than size of array
  while(interval < to_sort.get_size())
      interval = interval * 3 + 1;
  while(interval > 0) {
      interval_insertion(to_sort, interval);
      interval /= 3;
  }
}
