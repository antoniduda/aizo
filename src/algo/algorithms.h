#include "../collections/darray.h"
#pragma once

template <typename T> void insertion(DArray<T> to_sort);

template <typename T> void heap_sort(DArray<T> to_sort);
// Property for max heap
template <typename T> static bool property_max(T child, T parent);

// insertion at interval for shell sort
template <typename T> static void interval_insertion(DArray<T> to_sort, size_t interval);
template <typename T> void shell_shell(DArray<T> to_sort);
template <typename T> void shell_knuth(DArray<T> to_sort);


// enum specifying which element to chose as pivot
enum Pivot {
    First = 0,
    Last = 1,
    Random = 2,
    Middle = 3
};
// three functions below return the apropriet index of pivot based on size
size_t get_pivot_first(size_t size);
size_t get_pivot_last(size_t size);
// be sure to initialise seed before this
size_t get_pivot_random(size_t size);
size_t get_pivot_middle(size_t size);
// splits the array into 2 sections, one lower than pivot and one higher
template <typename T> size_t partition(DArray<T> to_sort, size_t pivot);
template <typename T> void quick(DArray<T> to_sort, Pivot p);

#include "heap_sort.inl"
#include "insertion.inl"
#include "shell.inl"
#include "quick.inl"
