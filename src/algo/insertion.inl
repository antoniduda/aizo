
template <typename T> void insertion(DArray<T> to_sort) {
  for (size_t i = 1; i < to_sort.get_size(); i++) {
    for (size_t j = i; j >= 1 && to_sort[j] < to_sort[j - 1]; j--) {
      // swap element with previous one untill the previous
      // element is bigger than current
      T tmp = to_sort[j];
      to_sort[j] = to_sort[j - 1];
      to_sort[j - 1] = tmp;
    }
  }
}
