#include <stdlib.h>

size_t get_pivot_first(size_t size) { return 0; }
size_t get_pivot_last(size_t size) { return size - 1; }
// be sure to initialise seed before this
size_t get_pivot_random(size_t size) { return rand() % size; }
size_t get_pivot_middle(size_t size) { return size / 2; }

// Array to much apropriate pivot selecting function based
// on Pivot enum
size_t (*pivot_arr[4])(size_t size) = {&get_pivot_first, &get_pivot_last,
                                       &get_pivot_random, &get_pivot_middle};

template <typename T> size_t partition(DArray<T> to_sort, size_t pivot) {
  size_t last_idx = 0;
  T tmp;
  // swap pivot with an element at the beginning of the array
  // so that we don't lose it's position
  tmp = to_sort[pivot];
  to_sort[pivot] = to_sort[0];
  to_sort[0] = tmp;
  pivot = 0;
  for (size_t i = 0; i < to_sort.get_size(); i++) {
    if (to_sort[i] <= to_sort[pivot]) {
      // if element is lower than pivot, 
      // then swap it with last element higher than pivot
      tmp = to_sort[i];
      to_sort[i] = to_sort[last_idx];
      to_sort[last_idx] = tmp;
      last_idx++;
    }
  }
  // move pivot to the "middle" of the array
  tmp = to_sort[last_idx - 1];
  to_sort[last_idx - 1] = to_sort[pivot];
  to_sort[pivot] = tmp;
  // return index of element spliting array
  return last_idx;
}

template <typename T> void quick(DArray<T> to_sort, Pivot p) {
  // if array is empty or size equal to one
  // then there is no point to perform partition
  // just return
  if (to_sort.get_size() <= 1)
    return;
  size_t pivot = pivot_arr[p](to_sort.get_size());
  // partition current array
  size_t part = partition(to_sort, pivot);

  // recure with sub arrays being left and right part of current array
  quick(to_sort.sub(0, part - 1), p);
  quick(to_sort.sub(part, to_sort.get_size()), p);
}
