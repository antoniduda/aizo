#include "../collections/heap.h"

template <typename T> static bool property_max(T child, T parent) {
  return parent > child;
}
template <typename T> void heap_sort(DArray<T> to_sort) {
  // create heap based on DArray
  Heap h(to_sort, &property_max<T>);
  while (h.get_size() > 0) {
    // put extracted maximum element of the heap at 
    // the freed position at the end of heap
    size_t tmp = h.get_size();
    to_sort[tmp - 1] = h.extract();
  }
}
