#pragma once

template <typename T>
Heap<T>::Heap(bool (*property)(T a, T b)) : DArray<T>::DArray((size_t)0) {
  this->property = property;
}

template <typename T>
Heap<T>::Heap(DArray<T> base, bool (*property)(T a, T b))
    : DArray<T>::DArray((size_t)0) {
  // Creates heap based on DArray
  this->property = property;
  // capacity should be the same as DArrays capacity
  this->capacity = base.get_capacity();

  // size is set to 1 temporarly
  this->size = base.get_size();
  this->p_array = base.get_array();
  // we perform sift up operation on all elements starting from left of heap to
  // ensure that heap property is met
  for (size_t i = this->get_parent(this->size - 1); i + 1 > 0; --i)
    sift_down(i);
}

template <typename T> void Heap<T>::insert(T n) {
  // insertion is esentialy pushing back
  // and then ensuring that heap property on parent of newly inserted element
  // is met
  this->push_back(n);
  sift_up(this->size - 1);
}

template <typename T> bool Heap<T>::is_right_child(size_t idx) {
  return idx % 2 == 0;
}

template <typename T> size_t Heap<T>::get_sibling(size_t idx) {
  if (is_right_child(idx)) // return left child if this is right
    return idx - 1;
  if (idx + 1 < this->size)
    return idx + 1;
  return 0;
}

template <typename T> size_t Heap<T>::get_parent(size_t idx) {
  return (idx - 1) / 2;
}

template <typename T> void Heap<T>::sift_up(size_t idx) {
  size_t parent = get_parent(idx);
  if (idx == 0 || property(this->p_array[idx], this->p_array[parent]))
    // if idx is 0 means we are root, and there is no parent where property
    // could be broken if propert(this, parent) is met there is no need to fix
    // anything
    return;

  size_t sibling = get_sibling(idx);
  if (sibling != 0 && !property(this->p_array[sibling], this->p_array[idx])) {
    // if there is a sibling and according to the property it should be
    // parent of current element, then we swap the sibling with parent
    // in that case we don't need to recure onto parent since it must meet the
    // property
    T tmp = this->p_array[sibling];
    this->p_array[sibling] = this->p_array[parent];
    this->p_array[parent] = tmp;
  }

  // in other cases we swap parent with ourselves and recur onto parent
  T tmp = this->p_array[parent];
  this->p_array[parent] = this->p_array[idx];
  this->p_array[idx] = tmp;
  sift_up(parent);
}

template <typename T> size_t Heap<T>::get_lchild(size_t idx) {
  idx = idx * 2 + 1;
  if (idx < this->size)
    return idx;
  return 0;
}

template <typename T> size_t Heap<T>::get_rchild(size_t idx) {
  idx = idx * 2 + 2;
  if (idx < this->size)
    return idx;
  return 0;
}

template <typename T> void Heap<T>::sift_down(size_t idx) {
  size_t left = get_lchild(idx);
  size_t right = get_rchild(idx);
  if (right) { // if there is a right child there also must be left child
    if (this->property(this->p_array[left], this->p_array[right])) {
      if (!this->property(this->p_array[right], this->p_array[idx])) {
        // if right child meets the property of being left childs parent
        // and the parent can't be right childs parent
        // swap right child with parent and recure onto right child
        T tmp = this->p_array[right];
        this->p_array[right] = this->p_array[idx];
        this->p_array[idx] = tmp;
        sift_down(right);
        return;
      }
    }
  }
  if (!left) // if there are no children return
    return;

  // in other cases, if current element can't be the parent
  // swap with left child and recur onto left child
  if (!this->property(this->p_array[left], this->p_array[idx])) {
    T tmp = this->p_array[left];
    this->p_array[left] = this->p_array[idx];
    this->p_array[idx] = tmp;
    sift_down(left);
  }
}

template <typename T> T Heap<T>::extract() {
  // return the top element, put lowest leaf on it's place and then sift down
  T value = this->p_array[0];
  this->p_array[0] = this->p_array[--this->size];
  this->sift_down(0);
  return value;
}
