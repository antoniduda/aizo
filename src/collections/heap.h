#include "darray.h"
#include "../types/ref_ptr.h"
#pragma once

template <class T> class Heap : public DArray<T> {
    // Heap implementation based on dynamic array
public:
  // Constructors take `property` as argument which should be a function
  // returning True if b can b a parent of a in the heap
  Heap(bool (*property)(T a, T b));
  Heap(DArray<T> base, bool (*property)(T a, T b));  // Constructor for creating
                                                     // heap out of array
  void insert(T n); // insets element at the end of heap and then
                    // uses sift_up to bring fix heap property
  T extract();      // removes root element of heap
private:
  bool is_right_child(size_t idx); // returns true if child at idx is a right child
  size_t get_sibling(size_t idx);   // return idx of sibling
  size_t get_lchild(size_t idx);    // returns left child idx if there is left child else 0
  size_t get_rchild(size_t idx);    // returns right child idx if there is right child else 0
  size_t get_parent(size_t idx);    // returns parent idx
  bool (*property)(T a, T b);
  void sift_up(size_t idx);         // fixes heap property on idx's parent and it's children
  void sift_down(size_t idx);  // fixes heap property on idx's children after extract operation
};

#include "heap.inl"
