#include <iostream>
#include <memory>
#include "../types/ref_ptr.h"

#pragma once

template <typename T> class DArray {
public:
  DArray(size_t n = 0);
  DArray(DArray<T> *to_copy);
  void push_back(T n);  // add element on the end of array (can realocate if necessary)
  T pop_back(); // pop element from back of array
  void resize(size_t s);
  T operator[](size_t i) const;
  T &operator[](size_t i);
  size_t get_size();
  size_t get_capacity();
  RefPtr<T> get_array();
  void clear(); // remove everything from array and free internal array
  void ensure_capacity(size_t n); // ensures there is enough space to fit n elements 
  const DArray<T> sub(size_t start, size_t end);    // return part of array from start to end

protected:
  RefPtr<T> p_array;
  size_t capacity = 0;  // specifies amount of space alloceted / T
  size_t size = 0;      // specifies amount of space currently used

private:
                                  // if not allocates it
  void copy_array(RefPtr<T> array_old, RefPtr<T> array_new);  // copies date from old array to new array
                                                // array_new must be larger or equal to array_old
};

#include "darray.inl"
