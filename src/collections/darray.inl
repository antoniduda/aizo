#pragma once

template <typename T> DArray<T>::DArray(size_t n) {
  p_array = RefPtr<T>::make(n);
  size = n;
  capacity = n;
}

template <typename T> DArray<T>::DArray(DArray<T> *to_copy) {
  this->capacity = to_copy->capacity;
  this->size = to_copy->size;
  this->p_array = to_copy->p_array;
}

template <typename T> size_t DArray<T>::get_size() { return size; }

template <typename T> T DArray<T>::operator[](size_t i) const {
  return p_array[i];
}
template <typename T> T &DArray<T>::operator[](size_t i) {
  return p_array[i];
}

template <typename T> void DArray<T>::ensure_capacity(size_t n) {
  if (capacity > n) // if there is enough space just return
    return;

  if (capacity * 3 / 2 > n) {
    // increase space by 1.5 if it's enough to fit new data
    capacity = capacity * 3 / 2;
    RefPtr<T> new_array = RefPtr<T>::make(capacity);
    copy_array(p_array, new_array);
    p_array = new_array;
    return;
  }

  capacity = n + 1;
  // if it's really necessary allocate enough data to fit n + 1 elements
  RefPtr<T> new_array = RefPtr<T>::make(capacity);
  copy_array(p_array, new_array);
  p_array = new_array;
}

template <typename T> void DArray<T>::push_back(T n) {
  ensure_capacity(size + 1);
  p_array[size] = n;
  size += 1;
}

template <typename T>
void DArray<T>::copy_array(RefPtr<T> array_old, RefPtr<T> array_new) {
  for (size_t i = 0; i < size; i++)
    array_new[i] = array_old[i];
}

template <typename T> void DArray<T>::resize(size_t s) {
  ensure_capacity(s);
  size = s;
}
template <typename T> void DArray<T>::clear() { this->size = 0; }

template <typename T> T DArray<T>::pop_back() { return p_array[--size]; }

template <typename T> size_t DArray<T>::get_capacity() { return capacity; }
template <typename T> RefPtr<T> DArray<T>::get_array() { return p_array; }

template <typename T> const DArray<T> DArray<T>::sub(size_t start, size_t end) {
  DArray<T> res;
  res.p_array = this->p_array.at(start);
  res.size = end - start;
  res.capacity = res.size; // capacity can be the same as size here
                           // since returned value is const it doesn't really
                           // matter as long as it's greater or equal to size
  return res;
}
