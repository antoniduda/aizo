#include "algo/algorithms.h"
#include <chrono>
#include <ratio>

template <typename T> void TypeMenu<T>::menu() {
  puts("test");
  // display menu in loop
  while (true) {
    std::cout << "Wybierz opcję:\n";
    std::cout << "1. Wczytaj plik\n";
    std::cout << "2. Wygeneruj dane 33 - posortowane, 66 - losowe\n";
    std::cout << "3. Wygeneruj dane 66 - posortowane, 33 - losowe\n";
    std::cout << "4. Wygeneruj dane 100 - losowe\n";
    std::cout << "5. Wyświetl obecną tablicę\n";
    std::cout << "6. Wyświetl ostatnio sortowaną tablicę\n";
    std::cout << "7. Sprawdź ostatnio sortowaną tablicę\n";
    std::cout << "8. Posortuj tablicę\n";
    std::cout << "9. Wyjdź\n";
    int choice;
    std::cin >> choice;
    switch (choice) {
    case 1:
      readFile();
      break;
    case 2:
      generate_30_60();
      break;
    case 3:
      generate_60_30();
      break;
    case 4:
      generate();
      break;
    case 5:
      display_data();
      break;
    case 6:
      display_result();
      break;
    case 7:
      check_sorted();
      break;
    case 8:
      sort();
      break;
    case 9:
      return;
    default:
      break;
    }
  }
}

template <typename T> void TypeMenu<T>::generate_30_60() {
  // generate random array and then sort first 1/3 of it
  std::cout << "Podaj liczbę liczb do wygnerowania: ";
  size_t n;
  std::cin >> n;
  size_t i = 0;
  data.clear();
  for (; i < n / 3; i++)
    this->data.push_back(this->min + i);
  size_t i_c = i;
  for (; i < n; i++)
    this->data.push_back(this->generate_random(this->min + i_c, this->max));
}

template <typename T> void TypeMenu<T>::generate_60_30() {
  // generate random array and then sort last 1/3 of it
  std::cout << "Podaj liczbę liczb do wygnerowania: ";
  size_t n;
  std::cin >> n;
  size_t i = 0;
  data.clear();
  for (; i < n * 2 / 3; i++)
    this->data.push_back(this->min + i);
  size_t i_c = i;
  for (; i < n; i++)
    this->data.push_back(this->generate_random(this->min + i_c, this->max));
}

template <typename T> void TypeMenu<T>::generate() {
  std::cout << "Podaj liczbę liczb do wygnerowania: ";
  size_t n;
  std::cin >> n;
  size_t i = 0;
  data.clear();
  for (; i < n; i++)
    this->data.push_back(this->generate_random(this->min, this->max));
}

template <typename T> void TypeMenu<T>::sort() {
  std::cout << "Wybierz metodę sortowania:\n";
  std::cout << "1. Insertion sort\n";
  std::cout << "2. Heap sort\n";
  std::cout << "3. Shell sort (Shell)\n";
  std::cout << "4. Shell sort (Knuth)\n";
  std::cout << "5. Quick sort (zerowy element pivotem)\n";
  std::cout << "6. Quick sort (ostatni element pivotem)\n";
  std::cout << "7. Quick sort (losowy element pivotem)\n";
  std::cout << "8. Quick sort (środkowy element pivotem)\n";
  int choice;
  std::cin >> choice;
  result.clear();
  result.ensure_capacity(this->data.get_size());
  for (size_t i = 0; i < this->data.get_size(); i++) {
    result.push_back(data[i]);
  }
  // pick sort based on choice
  const auto start = std::chrono::high_resolution_clock::now();
  switch (choice) {
  case 1:
    insertion(result);
    break;
  case 2:
    heap_sort(result);
    break;
  case 3:
    shell_shell(result);
    break;
  case 4:
    shell_knuth(result);
    break;
  case 5:
    quick(result, Pivot::First);
    break;
  case 6:
    quick(result, Pivot::Last);
    break;
  case 7:
    quick(result, Pivot::Random);
    break;
  case 8:
    quick(result, Pivot::Middle);
    break;
  default:
    return;
  }
  const auto end = std::chrono::high_resolution_clock::now();
  const std::chrono::duration<long int, std::nano> diff = end - start;
  std::cout << "Czas trwania sortowania " << diff.count() << "\n";
}

template <typename T> void TypeMenu<T>::display_data() {
  // display unsorted data separated by spaces
  for (size_t i = 0; i < data.get_size(); i++) {
    std::cout << data[i] << " ";
  }
  std::cout << "\n";
}

template <typename T> void TypeMenu<T>::display_result() {
  // display sorted array separated by spaces
  for (size_t i = 0; i < result.get_size(); i++) {
    std::cout << result[i] << " ";
  }
  std::cout << "\n";
}
template <typename T> void TypeMenu<T>::check_sorted() {
  // check if array got properly sorted
  for (size_t i = 1; i < result.get_size(); i++) {
    if (result[i - 1] > result[i]) {
      std::cout << "Błędny wynik\n";
      return;
    }
  }
  std::cout << "Poprawny wynik\n";
}
