#include "collections/darray.h"
#include <iostream>
#include <limits>

#pragma once

// base class with functions for thata generation
template <typename T> class TypeMenu {
public:
  // this needs to be overwritten, to read
  // the numbers in appropriate format
  virtual void readFile() {};
  void sort();
  void generate_30_60();
  void generate_60_30();
  void generate();
  void display_data();
  void display_result();
  void check_sorted();
  void menu();

protected:
  DArray<T> data;
  DArray<T> result;
  // this needs to be overridden, to generate
  // appropriate data type
  virtual T generate_random(T low, T high) = 0;
  T min;    // specifies min generated value
  T max;    // specifies max generated value
};

#include "type_menu.inl"
